package GUI;

import Bank.*;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class Serializare {
while( quit!=1)
    {
        try{
            BufferedReader obj = new BufferedReader(new InputStreamReader(System.in));
            System.out.print("Type 1 for Current Account and Any no for Saving Account : ");
            System.out.flush();
            str=obj.readLine();
            check_acct=Integer.parseInt(str);
        }
        catch(Exception e) {}

        if(check_acct==1)
        {
            do//For Current Account
            {
                System.out.println("\n\nChoose Your Choices ...");
                System.out.println("1) New Record Entry ");
                System.out.println("2) Display Record Details ");
                System.out.println("3) Deposit...");
                System.out.println("4) Withdraw...");
                System.out.println("5) Quit");
                System.out.print("Enter your choice :  ");
                System.out.flush();
                try{
                    BufferedReader obj = new BufferedReader(new InputStreamReader(System.in));
                    str=obj.readLine();
                    choice=Integer.parseInt(str);

                    switch(choice)
                    {
                        case 1 :  //New Record Entry
                            curr_obj.newEntry();
                            break;
                        case 2 :  //Displaying Record Details
                            curr_obj.display();
                            break;
                        case 3 : //Deposit...
                            curr_obj.deposit();
                            break;
                        case 4 : //Withdraw...
                            curr_obj.withdraw();
                            break;
                        case 5  :  System.out.println("\n\n.....Closing Current Account.....");
                            break;
                        default : System.out.println("\nInvalid Choice \n\n");
                    }
                }
                catch(Exception e)
                {}
            }while(choice!=5);
        }
        else
        {
            do//For Saving Account
            {
                System.out.println("Choose Your Choices ...");
                System.out.println("1) New Record Entry ");
                System.out.println("2) Display Record Details ");
                System.out.println("3) Deposit...");
                System.out.println("4) Withdraw...");
                System.out.println("5) Quit");
                System.out.print("Enter your choice :  ");
                System.out.flush();
                try{
                    BufferedReader obj = new BufferedReader(new InputStreamReader(System.in));
                    str=obj.readLine();
                    choice=Integer.parseInt(str);

                    switch(choice)
                    {
                        case 1 :  //New Record Entry
                            sav_obj.newEntry();
                            break;
                        case 2 :  //Displaying Record Details
                            sav_obj.display();
                            break;
                        case 3 : //Deposit...
                            sav_obj.deposit();
                            break;
                        case 4 : //Withdraw...
                            sav_obj.withdraw();
                            break;
                        case 5  :  System.out.println("\n\n.....Closing Saving Account.....");
                            break;
                        default : System.out.println("\nInvalid Choice \n\n");
                    }
                }
                catch(Exception e)
                {}
            }while(choice!=5);
        }

        try{
            BufferedReader obj = new BufferedReader(new InputStreamReader(System.in));
            System.out.print("\nEnter 1 for Exit : ");
            System.out.flush();
            str=obj.readLine();
            quit=Integer.parseInt(str);
        }catch (Exception e){}
    }
}
}
