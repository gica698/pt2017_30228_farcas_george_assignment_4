package Bank;

import java.io.Serializable;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;

public class Bank implements BankProc, Serializable {

    private Hashtable<Integer, Account> banca;

    public Bank() {
        banca = new Hashtable<Integer, Account>();
    }

    public Hashtable<Integer, Account> getBanca() {
        return this.banca;
    }

    public void setBanca(Hashtable<Integer, Account> banca) {
        this.banca = banca;
    }

    @Override
    public Account getAccount(int numar) {
        if (banca.containsKey(numar) == true) {
            assert banca.get(numar) != null : "Null exception";
            return banca.get(numar);
        }

        return null;
    }

    @Override
    public void addCont(Account cont) {
        assert cont == null : "Null exeception";
        banca.put(cont.getNumar(), cont);
        assert Verificare() == false : "Eroare la formare";
    }

    @Override
    public void removeCont(Account cont) {
        if (banca.containsKey(cont.getNumar()) == true) {
            banca.remove(cont.getNumar());
        }
        assert Verificare() == false : "Eroare la formare";
    }

    @Override
    public void addSuma(Account cont, double suma) {
        if (banca.containsKey(cont.getNumar()) == true) {
            cont.addDepozit(suma);
        }
        assert Verificare() == false : "Eroare la formare";
    }

    @Override
    public void removeSuma(Account cont, double suma) {
        if (banca.containsKey(cont.getNumar()) == true) {
            cont.removeDepozit(suma);
        }
        assert Verificare() == false : "Eroare la formare";
    }

    private boolean Verificare() {
        assert banca.isEmpty() == true : "Nu exista baza de date la banca";
        boolean rezultat = true;
        int i = 0, size = banca.size();
        Iterator<Map.Entry<Integer, Account>> it = banca.entrySet().iterator();
        Account contCurent;
        while (it.hasNext()) {
            i++;
            contCurent = (Account) it.next();
            if (contCurent == null) {
                rezultat = false;
            }
        }
        if (size != i) {
            rezultat = false;
        }
        return rezultat;
    }

}